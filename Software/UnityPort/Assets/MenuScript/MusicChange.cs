﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MusicChange : MonoBehaviour {
    public AudioSource menuMusic;
    public Toggle musicToggle;
    public Slider musicVolume;

    public void Slider_Changed(float newVolume)
    {
        menuMusic.volume = newVolume;
    }

    public void Toggle_Changed(bool musicToggle)
    {
        musicVolume.interactable = musicToggle;
        if (musicToggle == true)
            menuMusic.mute = false;
        else
            menuMusic.mute = true;
    }
    
}
