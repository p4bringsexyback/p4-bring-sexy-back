﻿using UnityEngine;
using System;
using System.IO;
using System.Net.Sockets;

public class TCPclientC : MonoBehaviour {

    int port = 7778;
    String host = "127.0.0.1";


    private TcpClient client;
    private NetworkStream stream;
    private StreamWriter writer;

    public void send(String command){
        writer.Write(command + " ;" + "\r\n");
        writer.Flush();
    }

    void setupTCP(){
        try{
            client = new TcpClient(host, port);
            stream = client.GetStream();
            writer = new StreamWriter(stream);
            Debug.Log("Socket ok");
        }
        catch (Exception e) {
            Debug.Log("Socket error: " + e);
        }
        }
    void Awake(){
        setupTCP();
    }
}