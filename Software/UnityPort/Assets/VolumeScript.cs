﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class VolumeScript : MonoBehaviour
{
    public AudioSource directionSound;
    public ArduinoPortScript arduinoInput;
    public int sensorNr;
    public Renderer rend;

    void Start(){
        GameObject g = GameObject.Find("_Manager");
        arduinoInput = g.GetComponent<ArduinoPortScript>();
        rend = GetComponent<Renderer>();
        rend.enabled = true;
    }
    void Update()
    {
        if (arduinoInput.inRange[sensorNr] == false)
        {
            directionSound.volume = 0;
            rend.enabled = false;
        }
        else {
            directionSound.volume = 1;
            rend.enabled = true;
        }
    }
}
