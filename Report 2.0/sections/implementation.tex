\chapter{Implementation}\label{ch:implementation}
In this section, the implementation of the different components of the \textit{Bat Prism} will be assessed, analyzed and explained thoroughly.

\section{Electronic Components}\label{sec:electronic_components}
As part of the implementation for the Bat Prism to aid non-sighted people, electronic components have been used, such as ultrasonic sensors, an Arduino Mega, a 9 V battery, and a bluetooth transmitter to enable the system to communicate with the computer wirelessly. In this section is an explanation of why the previously mentioned electronic components were chosen based on their technological attributes which are relevant for the Bat Prism to function.

\subsection{Ultrasonic Sensors}\label{subsec:ultrasonic}
An ultrasonic sensor uses the echolocation method, which is also utilized by the UltraCane, as described in section \ref{subsec:ultracane}. The time and speed it takes for the ultrasonic waves to travel from the transmitter to an object and back to the receiver is measured by the ultrasonic sensor. Figure \ref{fig:sensor_0} demonstrates how this works.\nocite{Aimagin}

\begin{figure}[!h]
  \centering
  \includegraphics[scale=0.6]{figures/sensor_0.png}
  \caption{The principle of the ultrasonic sensors (Saadi, aimagin website, 2014).}
  \label{fig:sensor_0}
\end{figure}

The surface material of the obstacles encountered by the sensor’s waves are also relevant when successfully calculating the echo sound created by said sensor. The smaller the area of the detected object, the harder they are to detect. Surfaces that work well with echolocation sensors are such things as: hard-surfaced cylinders, ball-shaped obstacles, and walls if horizontally faced directly by the sensor. When faced at a glancing angle, which is the opposite of a straight angle, walls will sound like they are further away from the sensors than they actually are, or will possibly not be detected at all, as can be seen in \ref{fig:sensor_1}. Soft materials, such as curtains, tend to not return sound that has been transmitted to it through the sensors.\nocite{Parallax}

\begin{figure}[!h]
   \centering
   \includegraphics[scale=0.6]{figures/sensor_1.png}
   \caption{Sources of error for the sensor (Lindsay, parallax website, 2013).}
   \label{fig:sensor_1}
\end{figure}

The ultrasonic sensors used for this project have four pins, as can be seen in figure \ref{fig:sensor_2}, which are places physically designated to connecting the sensors to the other electronic components. The four pins are:

\begin{itemize}
\item Vcc (orange cable in figure \ref{fig:sensor_2}): supplies 5 V to the Vcc pin.
\item Trig (green cable): input for a pulse width of 10 microseconds to transmit 40 kHz ultrasonic waves into the air from the transmitter.
\item Echo (blue cable): output pin for the echo signal that detects the width of the pulse, which is used to calculate the distance.
\item Gnd (black cable): ground pin.
\end{itemize}

\begin{figure}[!h]
   \centering
   \includegraphics[scale=0.8]{figures/sensor_2.png}
   \caption{The ultrasonic module HC-SR04, including four pins.}
   \label{fig:sensor_2}
\end{figure}

\subsection{Bluetooth Modem}
To transmit the data received from the Arduino, the device uses a Bluetooth modem called BlueSmirf Silver (see figure \ref{fig:sensor_3}). It takes an input between 3 and 6.6 V, and can transmit data in a range up to 18 m. The Bluetooth modem can be found by the computer’s bluetooth modem, where it will be taking up a specific port. A port is a logical connection through which the computer connects with another device. The connected computer can use this port to receive the data.\nocite{Sparkfun}

\begin{figure}[!h]
   \centering
   \includegraphics[scale=0.7]{figures/sensor_3.png}
   \caption{Bluetooth Modem (Sparkfun website, 2016).}
   \label{fig:sensor_3}
\end{figure}

\subsection{Arduino Mega 2560}
The Arduino Mega 2560 (see figure \ref{fig:arduino}), controls the software for the device. The device uses the Arduino Mega due to its large amount of digital pins. Those are needed, since each sensor takes two pins. The software on the Arduino Mega includes calculating the distance on all the ultrasonic sensors, combining them in a single string, and then writing them to the Bluetooth modem through a serial port --- this will be described in further detail in section \ref{sec:codeoverview}. This process happens each 100 ms.\nocite{Arduino}

\begin{figure}[!h]
   \centering
   \includegraphics[scale=0.4]{figures/arduino.png}
   \caption{Arduino Mega 2560 (Arduino website, 2016).}
   \label{fig:arduino}
\end{figure}

\section{Electronic schematics}\label{sec:schematics}
A schematic of the first build (before the prototype is expanded to contain eight sensors) can be seen in figure \ref{fig:schematic_0}, illustrating how the components are connected. Each ultrasonic sensor has four pins named \textit{vcc},  \textit{trig},  \textit{echo}, and \textit{ground}, respectively. In order to differentiate between the different pins, the wires are color coded. The black wires from the ultrasonic sensors connect their ground pin to the ground pin on the Arduino board. To give the component power, the orange wires are connected from the sensor’s vcc pin to the 5 V pin on the Arduino. In order to communicate with the ultrasonic sensors through a programming software, the trig and echo pins are connected to digital pins on the Arduino, where the green wires are used for the echo pin, and the blue wires are used for the trig pin.

\begin{figure}[!h]
   \centering
   \includegraphics[scale=0.29]{figures/schematic_0.png}
   \caption{The circuit with all the components connected.}
   \label{fig:schematic_0}
\end{figure}

The bluetooth modem uses six wires. The black and orange wires are once again used for the ground and the 5 V ports, respectively. The bluetooth component has an \textit{RX-0} pin, which is used to receive data. This pin is connected to a digital pin on the Arduino board using a purple wire --- specifically, it is connected to one of the communication pins, so that it can be used for serial communication. The bluetooth component also has a \textit{TX-1} pin, which is used to transmit data. This port is connected to a communication pin on the Arduino board using a gray cable, in order to communicate with it through a programming software.

\section{Software used}\label{sec:softwareused}
In order to program the actions of the different electronic components, the Arduino’s own programming software was used\nocite{Arduino}. This software receives variables returned by the sensors, and computes then to determine the location of a detected object. This information is then transmitted to the Unity portion of the software. 

Unity 3D\nocite{Unity} is a cross-platform game engine used for both the visual feedback (which is used for debugging), and for receiving information from Arduino and transmitting that to Pure Data. The language used for this part of the software is C\#.

This project also uses Pure Data (Pd)\nocite{PureData}, an open source visual programming language, which uses blocks of objects, messages, sliders, numbers, and symbols, amongst other things, to create audio effects applied to real time audio cues. Pure Data is used to manipulate variables received from Unity and create an audio feedback based on this information. This will be described in detail further into this chapter.

A blueprint of the final prototype is designed using Autodesk Inventor Professional 2017\nocite{Autodesk} and then 3D printed using a software called Cura 3D Printing Slicing Software\nocite{Ultimaker}.

\section{Code Overview}\label{sec:codeoverview}
To give an understanding of the code in Arduino, Pure Data and Unity, and how they interact with each other, an overview covering the code is provided. A flowchart (seen in figure \ref{fig:codeflowchartdiagram}) provides an overview of the different aspects of the software. It follows the code from the moment an obstacle is detected, to when the sound is sent to the speakers.

\begin{figure}[!p]
\centering
\includegraphics[width = \textwidth]{figures/codeFlowchartDiagram.png}
\caption{Flowchart providing an overview of the software.}
\label{fig:codeflowchartdiagram}
\end{figure}

The gray rectangles in the flowchart represent objects. In this flowchart, Pure Data patches are considered objects. The green \texttt{sensorScript} rectangle indicates the beginning of the flowchart. The diagram then continues via the arrows from object to object, with blue ellipses explaining what happens between the objects. The ellipses have numbers in them, indicating the order of the actions taken in the code, which is emphasized via the arrows. The end of the flowchart is the output, shown as a red diamond-shape. The objects seen in this flowchart will be explained in further detail later in this chapter.

\begin{figure}[H]
  \centering
  \includegraphics[width = \textwidth]{figures/sequence.png}
  \caption{The sequence diagram from Bat Prism’s software.}
  \label{fig:sequenceDiagram}
\end{figure}

To show the relationship between the objects, a sequence diagram is created, shown in figure \ref{fig:sequenceDiagram}. The diagram starts with the user on the left, labeled \textit{Non-sighted person} in the figure. It shows how they interact with the system, the feedback they get from the system, and the order in which the objects operate with one another.

\subsection{Arduino Code}\label{sec:arduinocode}
The process starts in the Arduino code, in \texttt{SensorScript}, as is shown in figure \ref{fig:sequenceDiagram}. The purpose of the Arduino’s software is to control the digital pins to get data from the sensors, as well as combining the data into a string, and sending that string to the remainder of the software. To get the data from the sensors, each sensor runs a \texttt{sensorOutput} function, which can be seen in listing \ref{lst:arduinoSensorCode}.

\begin{lstlisting}[caption={Function to calculate distance.}, label=lst:arduinoSensorCode, texcl=true, escapechar=$]
//the sensors' function to detect distance
long sensorOutput(int trigPin, int echoPin){
   long duration, distance;

   //turn off trigger pin for 2 μs
   digitalWrite(trigPin, LOW);
   delayMicroseconds(2);
  
   //turn on trigger pin for 10 μs, then off
   digitalWrite(trigPin, HIGH); $\label{ln:trighi}$
   delayMicroseconds(10);
   digitalWrite(trigPin, LOW);
  
   //measure duration from when waves are sent till they are received
   duration = pulseIn(echoPin, HIGH); $\label{ln:pulse}$
   distance = (duration/2) / 29.4; //convert to cm $\label{ln:cm}$
   return distance;
}
\end{lstlisting}

When the trigger pin is turned on in line \ref{ln:trighi}, ultrasonic waves are sent from the sensor’s transmitter, and the echo pin also turns on. The time it takes for the echo pin to turn off, which happens when the waves are detected by the receiver, is measured using the \texttt{pulseIn} function, as can be seen in line \ref{ln:pulse}. After the \texttt{duration} has been measured, it is converted to distance. This happens in line \ref{ln:cm}. The duration is divided by 2, since only half of the distance travelled by the waves is relevant. The resulting number is divided by 29.4. This is because sound travels at approximately 340 m/s, which corresponds to approximately 29.4 cm/μs.

The Bluetooth modem communicates using serial communication. To send the data at the fastest rate possible, the \textit{baud rate} is set to 115200, which is the highest rate at which the Bluetooth modem can send data. A baud rate at 115200 means that it transmits 115200 bits per second. The Arduino’s software, however, still has a delay of 100 ms. This is due to the fact that the sensors are not able to send and receive a new signal fast enough, if there is less delay.

To see the full code for Arduino see Appendix \ref{app:arduino}.

\subsection{Unity Code}\label{sec:unitycode}
After the data has been sent through the Bluetooth modem, Unity is set to pick up a specific port, which is the port the Bluetooth modem is connected to. The port needed for the Bluetooth depends on the computer it is connected to. Unity connects to the given port by using the function called \texttt{SerialPort}. It takes two inputs: a port and a baud rate. See listing \ref{lst:sp} for reference. This happens in \texttt{ArduinoPortScript}.

\begin{lstlisting}[caption={SerialPort connecting to a port.}, label=lst:sp]
sp = new SerialPort("\\\\.\\COM17", 115200, Parity.None, 8, StopBits.One);
\end{lstlisting}

Unity has a function for communicating with Arduino, \texttt{stream.ReadLine()}, which reads a single line sent from Arduino. However, when Unity reads from Arduino this way, if the program does not get anything to read, it will not execute further in the program, until a new signal is received. This essentially blocks the execution of the program. To work around this, an asynchronous waiting method is used, more specifically a coroutine. The coroutine performs quick reads alternated by quick waits. This is implemented in \texttt{ArduinoPortScript}.

In case of no signal being received within the given time period, the coroutine will send an error, as can be seen in listing \ref{lst:coroutine}.

\begin{lstlisting}[caption={Code for setting up coroutine.}, label=lst:coroutine]
void Update() {
//Gets the signal from the arduino, and performs all the operations.
       StartCoroutine(
               AsynchronousReadFromArduino
               ((string s) => Debug.Log(s), // Callback
               () => Debug.LogError("Error!"), // Error callback
               10f // Timeout (seconds)
               )
);
}
\end{lstlisting}

\texttt{AsynchronousReadFromArduino} is implemented in \texttt{ArduinoPortScript}. In this function, the received data string is split into an array with eight strings, one for each sensor, as can be seen in listing \ref{lst:recsend}, line \ref{ln:split}, and then parsed as an int in an array \texttt{tempSensorInput}, in line \ref{ln:parse}. If the detected distances are within range, they are saved in another array, \texttt{sensorInput} (seen in line \ref{ln:savearray}), and then sent to the Pure Data portion of the software, using a \texttt{TCPclientC} object (starting at line \ref{ln:clientsend}). If the distance is out of range, it is set to 0, as can be seen in line \ref{ln:zerodist}.

\begin{lstlisting}[caption={Code for receiving data from Arduino, parsing it, and sending it.}, label=lst:recsend, escapechar=|]
inputStr = sp.ReadLine(); //read data from Arduino
string[] strArray = inputStr.Split(','); //split into array of strings |\label{ln:split}|
for (int i = 0; i < strArray.Length; i++) {   
tempSensorInput[i] = Int32.Parse(strArray[i]); //parse strings as ints |\label{ln:parse}|

//if detected distance is within range, save in array
if (tempSensorInput[i] <= maxDistance) {
sensorInput[i] = tempSensorInput[i];|\label{ln:savearray}|
                        inRange[i] = true; 
}
//else save the distance as 0
else {
                        sensorInput[i] = 0; |\label{ln:zerodist}|
                        print("Too far away: " + i);
                        inRange[i] = false;
            }

//send detected distances through the TCPclientC |\label{ln:clientsend}|
client.send("sensorUpLeft " + sensorInput[0]);
client.send("sensorUp " + sensorInput[1]);
client.send("sensorUpRight " + sensorInput[2]);
client.send("sensorRight " + sensorInput[3]);
client.send("sensorDownRight " + sensorInput[4]);
client.send("sensorDown " + sensorInput[5]);
client.send("sensorDownLeft " + sensorInput[6]);
client.send("sensorLeft " + sensorInput[7]);
}
\end{lstlisting}

A full view of the \texttt{ArduinoPortScript} class can be seen in appendix \ref{app:portscript}.

The \texttt{client} object is of the class \texttt{TCPclientC}, which is a custom TCP client. It sets up a port for Pure Data to connect to. TCP is a form of communication through a server. Ports are used to make a connection between a server and a client, and are identified by a port number. This can be any number from 1023 to 65535. After a port number is chosen, the client initiates dialogue with the server, so that the client can receive transmissions from the server. For this program, the port chosen is 7778.

The data is transmitted by the server through a function called \texttt{send}, which takes a string as input. This string contains the name of a sensor, as well as a number indicating the distance detected by that sensor. The \texttt{send} function can be seen in listing \ref{lst:sendTCP}.
\newpage
\begin{lstlisting}[caption={Code for sending data to Pure Data.}, label=lst:sendTCP]
public void send(String command) {
        writer.Write(command + " ;" + "\r\n");
        writer.Flush();
}
\end{lstlisting}

A full view of the \texttt{TCPclientC} class can be seen in appendix \ref{app:tcp}.

\subsection{Audio Processing Methods}\label{subsec:audioprocessingmethods}
To create the audio feedback, \textit{Pure Data} is used. The data is sent from C\# and received in Pure Data by a \texttt{netreceive} object using the port declared in \texttt{TCPclientC}. This is done in the \texttt{NetworkReceiver} object (pictured in figure \ref{fig:netreceive}).

\begin{figure}[!h]
 \centering
 \includegraphics[width = \textwidth]{figures/netreceive.png}
 \caption{The \texttt{NetworkReceiver} patch in Pure Data.}
 \label{fig:netreceive}
\end{figure}

The received string is split into 8 numbers using the \texttt{route} object, each corresponding to the distance detected by a sensor. This information, as well as the corresponding angle and a sound signal, is processed using an \texttt{mPan} object, which will be explained further in this section. The five frontmost sensors result in a different sound than the three sensors in the back --- the front sensors result in the sound of a bell lasting 1 second, and the back sensors result in the sound of a gong lasting 6 seconds. Both are processed using the same methods. In figure \ref{fig:backfront}, the sensors considered front are marked in green, and the sensors considered rear are marked in red. Figure \ref{fig:backfront} also shows what angles the different sensors correspond to. Because of the way stereo panning is applied to a sound signal (which will be explained later), the sensors are not considered a full circle, but rather two half-circles, where the front-looking and back-looking directions are both considered zero radians. After the sound signal has been processed by the \texttt{mPan} patch, a boolean named \texttt{inRange} checks if an obstacle is within range. If so, the processed sound is played in the speakers.

\begin{figure}[!h]
 \centering
 \includegraphics[width = 0.7\textwidth]{figures/backfront.png}
 \caption{The device’s range and the angles at which sensors are placed.}
 \label{fig:backfront}
\end{figure}

The \texttt{mPan} patch handles the order in which the different effects are applied to the audio signal, as can be seen in figure \ref{fig:mpan}. First, \texttt{myWah} applies a \textit{wah}-effect to the signal. After the wah-effect is applied, distance and angle is simulated using \texttt{myDistance} and \texttt{myAngle} patches, and then the resulting signal is output back to \texttt{NetworkReceiver}.

\begin{figure}[!h]
 \centering
 \includegraphics[scale = 1]{figures/mPan.png}
 \caption{The \texttt{mPan} patch in Pure Data.}
 \label{fig:mpan}
\end{figure}

The \texttt{myWah} object uses a \textit{voltage control filter} (pictured in figure \ref{fig:mywah} as a \texttt{vcf~} object), which is a type of \textit{band-pass} filter. A band-pass filter allows some range of frequencies in a signal to be passed through it depending on a given \textit{center frequency} and \textit{resonance}. Signals of the center frequency will pass through the filter unchanged, and the further away a signal is from the center frequency, the more it is reduced. The resonance determines the width of the range of frequencies allowed to pass through the filter without being eliminated. The center frequency of the \texttt{vcf~} object can be controlled using an audio signal, allowing it to sweep over time. As can be seen in figure \ref{fig:mywah}, a low-frequency oscillator is used to sweep the center frequency, creating a wah-effect.

\begin{figure}[!h]
 \centering
 \includegraphics[scale = 1]{figures/myWah.png}
 \caption{The \texttt{myWah} patch in Pure Data.}
 \label{fig:mywah}
\end{figure}

The prototype aims to provide a feedback which makes it possible for the user to discern the location of the object which has been detected. To do this, \textit{spatial sound reproduction} must be implemented. Pulkki describes spatial sound reproduction as such:

\begin{quotation}
\textit{“The term ‘spatial sound reproduction’ denotes methods to reproduce or synthesize also the spatial attributes of sound to listeners”}(Pulkki, 2001, p. 11\nocite{Pulkki2001})
\end{quotation}

Firstly, the angle of the object relative to the device should be simulated through sound. The technique used in this project is \textit{amplitude panning}, in which two channels of a sound signal are applied to two speakers, \textit{n} and \textit{m}, with different amplitudes. These amplitudes are determined by the angle of the two speakers, and the angle from which the user should perceive the virtual source of sound. This angle is known as the \textit{perceived angle}, and is denoted in figure \ref{fig:stereo_panning} as \textit{\straighttheta\textsubscript{G}}. The direction in which the listener is facing is considered 0 radians (as was seen in figure \ref{fig:backfront}), and the angles of the two speakers are denoted \textit{\straighttheta\textsubscript{n}} and \textit{\straighttheta\textsubscript{m}}, respectively.

\begin{figure}[!h]
 \centering
 \includegraphics[width = 0.8 \textwidth]{figures/stereo_panning.png}
 \caption{Illustration of stereo panning.}
 \label{fig:stereo_panning}
\end{figure}

Since the device utilises earbuds, which are placed directly in the listener’s ears, \textit{\straighttheta\textsubscript{n}} is considered \( \frac{\pi}{2} \), and \textit{\straighttheta\textsubscript{m}} is considered \( -\frac{\pi}{2} \). Pulkki describes a panning law which allows one to determine the gain factor in speakers \textit{n} and \textit{m}, while setting their angles \textit{\straighttheta\textsubscript{n}} and \textit{\straighttheta\textsubscript{m}} (Pulkki, 2001, p. 13\nocite{Pulkki2001}):

\begin{equation}\label{eq:chowning}
\begin{aligned}
   & g_n = \sqrt{\frac{\theta_m - \theta_G}{\theta_m - \theta_n}}\\
   & g_m = \sqrt{\frac{\theta_n - \theta_G}{\theta_n - \theta_m}}
\end{aligned}
\end{equation}

In equations \ref{eq:chowning}, \textit{g\textsubscript{n}} and \textit{g\textsubscript{m}} are the gain factors to be applied to the signal in speakers \textit{n} and \textit{m}, respectively. This technique is the basis of the \texttt{myAngle} effect used in the software. The Pure Data code for this effect can be seen in figure \ref{fig:angle_filter}. Here, an angle relative to the forward looking direction is given in radians as an \texttt{inlet}, which in Pure Data means an input. The panning law is applied in the two \texttt{expr} objects, resulting in two float values, which are the gain factors \textit{g\textsubscript{n}} (for the left channel) and \textit{g\textsubscript{m}} (for the right channel).

\begin{figure}[H]
 \centering
 \includegraphics[width = 0.9 \textwidth]{figures/angle_filter.png}
 \caption{The \texttt{myAngle} patch in Pure Data.}
 \label{fig:angle_filter}
\end{figure}

Distance also needs to be simulated. This is done using a \texttt{myDistance} object, shown in figure \ref{fig:distance_filter}. Because of the device’s range mentioned in section \ref{sec:electronic_components}, the distance to an object will range from 2 cm to 15 cm. The distance effect will be exaggerated in the audio feedback, in order to make differences in distance more pronounced. The effect is achieved using three concepts: a lowpass filter, a reverb effect, and amplitude.

A lowpass filter (seen in figure \ref{fig:distance_filter} as the \texttt{lop\midtilde} function) cancels out any signal that is above the given cut-off value in hertz. The reason that a lowpass filter is used here is that low frequency signals are able to travel farther through air than high frequency signals. As can be seen in figure \ref{fig:distance_filter}, the cut-off value is calculated by normalizing the given distance to range from 800 to 200 using the topmost \texttt{expr} object --- the greater the distance, the lower the cut-off value, thus allowing only low frequency signals to pass through. If the given distance is out of range (lower than 2 or higher than 15 cm), the cut-off value is set to 0, thus allowing no sound to pass through the lowpass filter.

\begin{figure}[!h]
 \centering
 \includegraphics[width = \textwidth]{figures/distance_filter.png}
 \caption{The \texttt{myDistance} patch in Pure Data.}
 \label{fig:distance_filter}
\end{figure}

\newpage
The next thing which needs to be done to simulate distance is changing the amplitude. To do this, a gain factor is calculated by normalizing the distance to a float ranging from 1 to 0 (calculated in the second \texttt{expr} function in figure \ref{fig:distance_filter}), so that the shortest distance results in a gain factor of 1, and the longest distance results in a gain factor of 0. If the given distance is out of range, the gain factor is set to 0.

Lastly, a reverb filter (seen in figure \ref{fig:distance_filter} as the \texttt{freeverb\midtilde} function) is used to control the \textit{reverberation} of the sound, meaning the persistence of the sound after it is produced. This is to simulate waves being reflected, causing reflected sound to build up and decay. Reverberation time is the time it takes for the reflected sound waves to decay, until they cannot be heard at all. The desired effect for this solution is that as the device moves farther from an obstacle, the reverberation time increases, and a larger fraction of the signal is reverbed. The reverberation time is controlled with the \texttt{roomsize} message, which increases the reverberation time as the gain factor grows smaller. The fraction of the signal which is reverbed is controlled with the \texttt{dry} and \texttt{wet} messages. The \texttt{wet} message controls how large a fraction is processed, and the \texttt{dry} message controls how large a fraction is not. These depend on the previously determined gain factor --- the closer the obstacle, the higher the gain factor, and the higher the dry level. The wet level grows as the gain factor grows smaller.

\begin{figure}[!h]
 \centering
 \includegraphics[scale=0.7]{figures/inRange.png}
 \caption{The \texttt{inRange} patch in Pure Data.}
 \label{fig:inrange}
\end{figure}

Once the effects have been applied, an \texttt{inRange} patch checks if an obstacle is in range, and if not, blocks the sound signal from being sent to the speakers. The \texttt{inRange} object, as can be seen in figure \ref{fig:inrange}, takes two signal inlets, as well as a number inlet, which is the distance in cm. This number is compared to both 2 and 15. If the number is between 2 and 15, both channels get a gain factor of 1. If the number is higher than 15 or lower than 2, the signal receives a gain factor of 0, eliminating all sound, including remaining echo from the distance effect.

\section{Test run of Hardware}\label{sec:test_run_of_hardware}
To ensure the hardware’s reliability and foresee possible errors, all of the possible errors stated in the official documentation of the device’s components have been assessed, and each component has been tested individually. These tests are conducted on the early iteration of the prototype mention in chapter \ref{ch:design}. It can be seen in figure \ref{fig:woodprot}.

\begin{figure}[!h]
 \centering
 \includegraphics[width = 0.3 \textwidth]{figures/primitive.jpg}
 \caption{The early prototype on which the hardware tests are conducted.}
 \label{fig:woodprot}
\end{figure}

This version of the device consists of the following electronic components:

\begin{itemize}
\item 1x BlueSmirf Silver
\item 1x Arduino Mega 2560
\item 1x 3.3 V Lithium battery
\item 1x Adafruit PowerBoost 500
\item 4x PING))) Ultrasonic Distance Sensor
\end{itemize}

\subsection{BlueSmirf Silver}\label{sec:bluesmirf_silver}
The Bluesmirf Silver’s range is 18 m\nocite{Sparkfun}, which is suitable for testing. It has an operating voltage of anything between 3.3 V – 6 V.

To ensure that the BlueSmirf Silver works as documented, a short test is conducted. The bluetooth modem’s port is opened, and data is received by a computer. Four ultrasonic sensors are used for this test. As long as an object is within range of the device, the BlueSmirf sends a “within range” string to the computer, to be printed on the console. The test shows that while a computer can receive data from the BlueSmirf whenever it is within a range of 18 m, the rate at which data is received drops after 10 m. To receive data efficiently, the BlueSmirf must stay within a range of 10 m from the computer. Since the device is built for small-scale testing, this should not cause any issues when testing the device.

\subsection{Arduino Mega 2560}\label{sec:arduino_mega_2560}
The Arduino Mega controls the software on the device. To do this, the Arduino has several digital pins which can take an input or an output voltage. Furthermore, it has pins for communication, called RX (Receiving data pins) and TX (Transmitting data pins). It also gives 5 V to each component, and provides a ground pin. For the device to work, the Arduino Mega must be powered and running.

\subsection{Adafruit Powerbooster, Lithium Battery, and Switch}\label{sec:adafruit_power_booster}
The electronic components need to be powered by 5 V, but the lithium battery only outputs 3.3 V. Therefore, the Powerbooster is needed to increase the power given by the lithium battery to 5 V or more. In this case, the Powerbooster outputs 5.2 V to the Arduino. The Powerbooster is tested alongside the battery. It is observed whether the two components power the device as intended, which they do. The Powerbooster provides a way to turn the battery usage off, by connecting two pins on the Powerbooster. Those pins are connected using a 2-way switch. When the switch is flipped one way, of the button is off, and the other way it is on. This was also tested, and works as intended. These components are not a part of the final prototype, as the battery has been replaced by a 9 V battery. The lithium battery, Powerbooster, and switch were, however, a part of the initial hardware tests described in this chapter.

\subsection{PING))) Ultrasonic Distance Sensor}\label{sec:ultrasonic_distance_sensor}
The ultrasonic distance sensors each calculate a distance, which is converted into a single string to be transmitted to the computer. They should be able to detect from 2 cm to 300 cm, as long as the transmitted waves hit an even surface. Each of the sensors are tested at a distance of 300 cm. In table \ref{tbl:distance}, the results of this test can be seen. Here, both the detected distance and the total time it took for the signal to return are displayed.

Further testing shows that the sensor can detect objects at a distance up to 500 cm. However, at this distance, the slightest dent in the surface it is detecting can throw the sonar sound off course, which causes the sensor to receive no signal.

\begin{center}
\begin{table}[!h]
 \begin{tabularx}{\textwidth}{| X | X | X | X | X | X | X | X |}
 \hline
\multicolumn{2}{|c|}{Sensor front} & \multicolumn{2}{|c|}{Sensor right} & \multicolumn{2}{|c|}{Sensor back} & \multicolumn{2}{|c|}{Sensor left} \\ \hline
Detected distance (cm) & Total time (ms) & Detected distance (cm) & Total time (ms) & Detected distance (cm) & Total time (ms) & Detected distance (cm) & Total time (ms)\\ \hline
301 & 18 & 302 & 18 & 303 & 18 & 300 & 17 \\ \hline
302 & 19 & 301 & 17 & 303 & 18 & 300 & 18 \\ \hline
302 & 19 & 301 & 18 & 303 & 18 & 300 & 18 \\ \hline
302 & 18 & 301 & 18 & 302 & 18 & 300 & 19 \\ \hline
302 & 18 & 300 & 19 & 302 & 19 & 300 & 19 \\ \hline
302 & 19 & 301 & 19 & 302 & 18 & 300 & 18 \\ \hline
302 & 19 & 301 & 18 & 303 & 18 & 301 & 18 \\ \hline
301 & 18 & 300 & 18 & 303 & 18 & 300 & 19 \\ \hline
302 & 18 & 301 & 18 & 302 & 19 & 302 & 19 \\ \hline
303 & 18 & 301 & 19 & 303 & 18 & 302 & 18 \\ \hline
301 & 19 & 301 & 19 & 303 & 18 & 302 & 18 \\ \hline
302 & 19 & 300 & 17 & 302 & 18 & 301 & 18 \\ \hline
301 & 18 & 300 & 17 & 302 & 19 & 300 & 19 \\ \hline
302 & 18 & 300 & 19 & 302 & 19 & 301 & 19 \\ \hline
302 & 18 & 301 & 19 & 302 & 18 & 301 & 18 \\ \hline
 \end{tabularx}\caption{Results from the hardware test of the ultrasonic sensors. \label{tbl:distance}}
 \end{table}
\end{center}

The sensors are also tested for their reaction if no object is within range. In this case, the sensors send a number between 3000 cm and 3250 cm after approximately 100 ms. When such signals are received, it is important to ensure that the software will ignore them, to avoid errors.
Further testing, including all sensors at once, showed that each sensor waits approximately 100 ms if no signal is received. In the worst case scenario, the device would therefore take approximately 100 ms multiplied by the amount of sensors, when none of the sensors detect anything.

Lastly, the sensors’ ability to detect a skewed surface is tested. To do this, a single sensor is tested on objects skewed at angles 10\textdegree, 20\textdegree, 30\textdegree, and 40\textdegree, at the ranges 5 cm, 10 cm, and 15 cm. For results, see table \ref{tbl:skew}.

\begin{center}
\begin{table}[H]
	\centering
   \begin{tabular}{| l | l | l | l |}
   \hline
   \textbf{Real distance:} & \textbf{5 cm} & \textbf{10 cm} & \textbf{15 cm} \\ \hline
   \textbf{10\textdegree} & 0 cm & 0 cm & 0 cm \\ \hline
   \textbf{20\textdegree} & 1 cm & 1 cm & 2 cm \\ \hline
   \textbf{30\textdegree} & 1 cm & 2 cm & - \\ \hline
   \textbf{40\textdegree} & 2 cm & - & - \\ \hline
   \end{tabular}\caption{Test results for a sensor detecting skewed surfaces. \label{tbl:skew}}
\end{table}
\end{center}

When transmitting waves at slightly skewed surfaces, the sensors may return a number which is higher than the actual distance to the object. Table \ref{tbl:skew} shows how large an error (in centimetres) is returned when detecting objects at three different distances, skewed at four different angles. As can be seen in these results, the distance can be expected to have an error of about 1-2 cm, on surfaces that are skewed, as distance increases. As the distance and skewed angle increase, the sensors cease to return a number at all.

\subsection{Implementation Conclusion}\label{sec:implementation_conclusion}
With the implementation chapter, the hardware and software created from the design concept results in the following final design that was built as shown in figure \ref{fig:finished_design}. This prototype will be used in the upcoming evaluation chapter, where there will be three experiments corresponding to the three hypotheses stated in chapter \ref{ch:finalproblem}.

\begin{figure}[!h]
 \centering
 \includegraphics[width = 0.3 \textwidth]{figures/finished_design.png}
 \caption{Final design, prototype.}
 \label{fig:finished_design}
\end{figure}