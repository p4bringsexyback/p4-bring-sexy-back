\chapter{Experimentation and Evaluation}\label{ch:evaluation}
In chapter \ref{ch:finalproblem}, three hypotheses are set up. Recall that the purpose of this study is to examine the effects an audio map may have on a non-sighted person's ability to create cognitive maps of their surroundings. In this chapter, it is evaluated how well the purpose of the study is addressed by dividing the evaluation of the solution into three experiments, each corresponding to a hypothesis stated in chapter \ref{ch:finalproblem}.

\section{Experiment for Hypothesis 1 --- Evaluation of the Device}\label{sec:1hypoExperiment}
As seen in chapter \ref{ch:finalproblem}, the first hypothesis states the following:

\paragraph{Hypothesis 1:} The device is able to determine the location of an object relative to itself both by angle and by distance.\\
\\
From this, a null hypothesis and an alternative hypothesis can be written as follows:

\paragraph{H\textsubscript{0} (Null hypothesis):} The success rate for detecting the correct distance to an object is 0.95 or less.


\paragraph{H\textsubscript{a} (Alternative hypothesis):} The success rate for detecting the correct distance to an object is more than 0.95.

\subsection{Experiment Procedure}
The sample data comes from testing how well the device detects obstacles. The sample consists of 64 detection attempts; eight attempts for each of the eight sensors.

\begin{figure}[!h]
    \centering
    \includegraphics[width=0.5 \textwidth]{figures/1hypoP1.png}
    \caption{Picture of set up, seen from the side.}
    \label{fig:1hypoP1}
\end{figure}

\begin{figure}[!h]
    \centering
    \includegraphics[width=0.5 \textwidth]{figures/1hypoP2.png}
    \caption{Picture of set up, seen from above.}
    \label{fig:1hypoP2}
\end{figure}

The setup for this experiment can be seen in figures \ref{fig:1hypoP1} and \ref{fig:1hypoP2}. The sensors will each be tested by placing a vertical cardboard plane in front of them at the distances 2, 3, 6, 7, 9, 12, 13, and 15 cm, one by one. Those eight distances will be referred to as the \textit{correct value}, and the distance detected by the device will be referred to as the \textit{detected value}. Comparing these yields two possible response values: \textit{success} (if the detected value is the same as the correct value) and \textit{failure} (if they are different). This makes the test for this hypothesis a \textit{binomial test}.

\subsection{Analysis of Results}
The binomial test will be testing for whether the detected distance was a failure or a success --- in other words, whether the sensor failed to detect the correct distance to the object or succeeded. The results gathered in the experiment can be seen in table \ref{tbl:hyp1}. The experiment yielded 64 successes out of 64 trials.

\begin{table}[!h]
\begin{center}
\begin{tabular}{| l | l | l | l | l | l | l | l | l |}
\hline
\textbf{Correct value:} & \textbf{2} & \textbf{3} & \textbf{6} & \textbf{7} & \textbf{9} & \textbf{12} & \textbf{13} & \textbf{15} \\ \hline
\textbf{Sensor 1} & 2 & 3 & 6 & 7 & 9 & 12 & 13 & 15 \\ \hline
\textbf{Sensor 2} & 2 & 3 & 6 & 7 & 9 & 12 & 13 & 15 \\ \hline
\textbf{Sensor 3} & 2 & 3 & 6 & 7 & 9 & 12 & 13 & 15 \\ \hline
\textbf{Sensor 4} & 2 & 3 & 6 & 7 & 9 & 12 & 13 & 15 \\ \hline
\textbf{Sensor 5} & 2 & 3 & 6 & 7 & 9 & 12 & 13 & 15 \\ \hline
\textbf{Sensor 6} & 2 & 3 & 6 & 7 & 9 & 12 & 13 & 15 \\ \hline
\textbf{Sensor 7} & 2 & 3 & 6 & 7 & 9 & 12 & 13 & 15 \\ \hline
\textbf{Sensor 8} & 2 & 3 & 6 & 7 & 9 & 12 & 13 & 15 \\ \hline
\end{tabular}\caption{Data from testing sensors. \label{tbl:hyp1}}
\end{center}
\end{table}

To compute this data, the \texttt{binom.test} function in R is used. The inputs this function takes are, in order: The amount of successes, the amount of trials, the desired minimum success rate, and whether the alternative hypothesis states that the success rate should be higher or lower. The command for this particular binomial test is thus \texttt{binom.test(64,64,(0.95), alternative="greater")}. Using this command to compute the data shows that the probability of success is 1, and the p-value is 0.03752. Since p<0.05, there is significant evidence against the null hypothesis, which can thus be rejected. 

\subsection{Conclusion and Discussion}
The experiment indicates that the sensors are able to detect the correct distance to a vertical cardboard plane. In the experiment, eight different distances are detected, covering the device’s range from the closest possible distance to the farthest. The fact that each measured distance is tested on every sensor ensures that the experiment is balanced. Other points to be taken into consideration are the angles. There are a total of eight sensors on the final design, arranged in a circle with 45\textdegree \ between one another. It makes for a few spaces in which accurate detection is difficult, and with possibly undetectable objects in between the sensors, if they are either too small or thin. Furthermore, as seen in section \ref{sec:test_run_of_hardware}, the ultrasonic sensors are not able to detect skewed surfaces correctly. The skewed surface would make a deviation of 1-2 cm, if the surface is even detected at all. These errors can cause the wrong kind of feedback, and therefore give the wrong indication of distance to objects surrounding a user of the device.

\section{Experiment for Hypothesis 2 --- Evaluation of the Audio Map}
As seen in chapter \ref{ch:finalproblem}, the second hypothesis states the following:

\paragraph{Hypothesis 2:} Non-sighted individuals who listened to the audio map in advance are better at following a route while using the device, than those who did not listen to the audio map.\\
\\
Given the chosen significance value of 0.05, a null hypothesis, as well as an alternative hypothesis, can be written as follows:
    
\paragraph{H\textsubscript{0} (Null hypothesis):} Non-sighted individuals who did listen to the audio map in advance are not better at following a route while using the device, than those who did not listen to the audio map.

\paragraph{H\textsubscript{a} (Alternative hypothesis):}Non-sighted individuals who did listen to the audio map in advance are better at following a route while using the device, than those who did not listen to the audio map.

\subsection{Experiment Procedure}
In this experiment, participants are asked to use the Bat Prism to navigate through an environment without the use of sight. Their purpose is to follow an intended route which half of them heard in an audio map. Whether or not they followed this route correctly is then judged to be either right or wrong.

This will be a two-sample binomial experiment where the design of \textit{treatment vs. control group} will be utilized. The goal with this experiment is to analyze if the treatment works --- in this case, \textit{treatment group} refers to the group that are exposed to a pre-recorded audio map of the route which they will have to follow with the Bat Prism. The control group is the group not receiving the treatment of listening to an audio map. The two samples are from the same population, consisting of people who have been blindfolded, to simulate non-sightedness. In total, 20 people participated in the experiment; 10 in the treatment group, and 10 in the control group.

The audio map is an audio file which has been recorded before the test using the Bat Prism. The sounds heard in the map correspond to the sounds which will be produced by the device if it follows the same route as represented by the map. To give the test participant an understanding of the device before the test itself begins, they will be presented with a small environment, in which they can test how the feedback sounds with respect to obstacles. This happens while they are blindfolded. In figure \ref{fig:expe_setup}, the full setup can be seen. The audio map was recorded in the main testing environment seen to the left in the picture. This environment is where participants attempt to follow the correct route, which will either be to the left or to the right in the T-shaped environment. To the right in the picture is the small testing environment, where users learn how to use the device and get used to the sounds coming from the device before moving on to the main environment. None of the test participants know what the test environment looks like.

\begin{figure}[!h]
    \centering
    \includegraphics[width = \textwidth]{figures/expe_setup.png}
    \caption{Experiment setup for hypothesis 2, with the main testing environment on the left, and the small pre-test environment on the right.}
    \label{fig:expe_setup}
\end{figure} 

The test is conducted by a test facilitator and a note taker, who is also responsible for setting up the software. The participants that are a part of the \textbf{treatment group} went through the following testing steps:

\begin{enumerate}
\item They get an explanation of how the test will be performed, and what they will do once inside the testing room.
\item The test facilitator blindfolds the participant and helps them inside the testing room.
\item The participant then tests the device in the small testing environment, to get an idea of how the audio feedback works.
\item The participant then listens to the audio map twice.
\item The participant is asked to try to recreate the audio map by hovering the device inside the main testing environment. The participants are specifically told to hover the device, as it may otherwise get caught in the wooden floor of the environment.
\item The participant then has two minutes to reach the end of the environment. They can terminate the test themselves if they think they have reached the end.
\item Throughout the test, the note taker observes the user and notes down the results. 
\item If the participant successfully follows the path given by the audio map, the response from that trial is noted as a success.
\end{enumerate}

Test participants that are a part of the \textbf{control group} went through the following testing steps:

\begin{enumerate}
\item They get an explanation of how the test will be and what they will do once inside the testing room.
\item The test facilitator blindfolds the participant and helps them inside the testing room.
\item The participant tests the device in the small testing environment, to get an idea of how the audio feedback works. 
\item This group of participants are not exposed to an audio map, hence they have no knowledge of the environment they are going to traverse with the device, even though the environment is the same as the one in which the treatment group tested.
\item The participant is asked to hover the device through the main testing environment and find the end of the environment.
\item The participant has two minutes to reach the end of the environment. They can also terminate the test themselves if they think they reached the end.
\item Throughout the test, the note taker observes the user and notes down the results. 
\item The participants of the control group would be noted as having successfully reached their target in the main testing environment if the spot where the experiment stops is on the correct route --- whether this is to the right or left is determined beforehand by the test facilitator.
\end{enumerate}

Once the two groups have completed the experiment, the responses from both groups are evaluated to test for differences. The variables yielded from the experiment make up for a \textit{binomial experiment}, where the response variables are either \textit{correct} or \textit{wrong}. A response is considered \textit{correct} when users find the correct path according to the directions in the given audio map, or in the case of the control group, the predetermined correct route. A response is considered \textit{wrong} when users do not find the correct path. 

The criteria for accepting a test as ‘correct’ for the treatment group was based upon the users moving to the right when having been exposed to an audio map recorded with the device moving to the right, or moving to the left when having been exposed to an audio map recorded with the device moving to the left. If a trial ended before one of the ends of the environment had been reached, the response is considered correct if the device ends on top of the correct track. If not, it is considered a failure.

\begin{figure}[!h]
    \centering
    \includegraphics[width = 0.6 \textwidth]{figures/Tshape.png}
    \caption{An illustration of the main testing environment, seen from above.}
    \label{fig:Tshape}
\end{figure}

As can be seen in figure \ref{fig:Tshape}, the main testing environment is a T-shape with two path choices; right or left. The participant will not know its layout, since they are blindfolded.

\subsection{Analysis of Results}
The data in tables \ref{tbl:hyp2_both} comes from the experiments conducted with the treatment group (table \ref{tbl:hyp2_treatment}) and the control group (table \ref{tbl:hyp2_control}), respectively.

\begin{table}[!h]
\centering
\begin{subtable}{0.3\textwidth}
 \begin{tabular}{| l | l | l |}
 \hline
 \textbf{Track} & \textbf{Correct} & \textbf{Wrong} \\ \hline
 \textbf{R} & X & - \\ \hline
 \textbf{R} & - & X \\ \hline
 \textbf{L} & X & - \\ \hline
 \textbf{R} & - & X \\ \hline
 \textbf{L} & X & - \\ \hline
 \textbf{R} & X & - \\ \hline
 \textbf{L} & X &  - \\ \hline
 \textbf{L} & X & - \\ \hline
 \textbf{R} & X & - \\ \hline
 \textbf{L} & - & X \\ \hline
 \end{tabular}\subcaption{Results from the treatment group. \label{tbl:hyp2_treatment}}
\end{subtable}\hspace{50 pt}
\begin{subtable}{0.3\textwidth}
 \begin{tabular}{| l | l | l |}
 \hline
 \textbf{Track} & \textbf{Correct} & \textbf{Wrong} \\ \hline
 \textbf{R} & - & X \\ \hline
 \textbf{R} & . & X \\ \hline
 \textbf{L} & X & - \\ \hline
 \textbf{R} & - & X \\ \hline
 \textbf{L} & - & X \\ \hline
 \textbf{R} & - & X \\ \hline
 \textbf{L} & X &  - \\ \hline
 \textbf{L} & - & X \\ \hline
 \textbf{L} & X & - \\ \hline
 \textbf{R} & - & X \\ \hline
 \end{tabular}\subcaption{Results from the control group. \label{tbl:hyp2_control}}
\end{subtable}\caption{Table showing the amount of correct and wrong routes takes by the treatment group (who have listened to the audio map) and the control group (who have not listened to the audio map)\label{tbl:hyp2_both}}
\end{table}

A significance level of  \textalpha = 0.05 has been chosen for this experiment. The treatment group had 7 successful trials out of a total of 10, and the control group had 3 successful trials out of 10. To compare the two groups, the R function \texttt{prop.test} is used. The command used for doing this is \texttt{prop.test(mtrx,alternative="greater")}, where \texttt{mtrx} is a matrix containing the amount of successes and failures of the treatment and the control group, respectively. Using this command to compute the data shows that the probability of success for the two groups are 0.7 for the treatment group, and 0.3 for the control group. The treatment group thus has a higher probability of success than the control group, as the hypothesis states. However, using the \texttt{prop.test} command also shows that the p-value is 0.08986. Since p>0.05, the results cannot be deemed significant, and the null hypothesis cannot be rejected.

\subsection{Conclusion and Discussion}
The data obtained from the experiment analysis shows that while a difference in performance is observed between the group that listened to an audio map and the group that did not, this difference is not significant enough to reject the null hypothesis. It can therefore not be concluded that the audio map allows blindfolded people to better follow a route than people who did not listen to an audio map.

A larger sample size may improve the significance of the results; using the \texttt{prop.test} function in R with twice as many trials (assuming that the proportion of successful trials remains the same) yields a p-value of 0.01343, which is lower than the significance level. In other words, more reliable results may have been achieved by increasing the sample size.

An issue which is voiced by several test participants is the size of the main testing environment. Since the end of the track is outside of an arm’s reach, the size of the environment limited the participants’ movement, possibly influencing the results. Another factor which limited the movement of some participants is the fact that the device is not equipped with a handle, and therefore several people used two hands to hold the device. Participants who held the device this way were less likely to reach the end of the track.

In the treatment group, nobody reached the end of the environment, even though this group had more successful attempts at finding the correct path to follow than the control group. In this group, three people reached the end. An explanation for this may be that participants in the control group are more inclined to move the device without care for the environment, and therefore find the end of it, while participants in the treatment group may be inclined to carefully try to imitate the sound they heard in the audio map. It is also possible that the participants in the treatment group tried to recreate the sound they had heard in the audio map but were not able to.

At some points during the test, the device got stuck in the wooden surface of the testing environment, making some participants think that they had hit a wall in the environment, when in reality they had not. This may have influenced the results and caused some error.

Another issue voiced by some participants is that they had trouble remembering the entire audio map while testing the device. It is also possible that participants were not able to discern between feedback for objects detected at different directions. This is tested in the last experiment.

\section{Experiment for Hypothesis 3 --- Evaluation of the Audio Feedback}
To test whether the user's ability to discern between feedback coming from different directions may have had an effect on the results of the previous experiment, the following test is conducted. In total, there are eight directions from which the virtual sound may be coming. As seen in chapter \ref{ch:finalproblem}, the third hypothesis states the following:

\paragraph{Hypothesis 3:} The audio feedback provided for objects detected in different directions relative to the device is clearly distinguishable from one another.\\
\\
In order to work with this hypothesis, a null- and an alternative hypothesis must be specified. Since distinguishing between audio feedback can be seen as a discriminatory action, the test conducted for these hypotheses will be binomial, which is why the hypotheses are the following:

\paragraph{H\textsubscript{0} (Null hypothesis):} When comparing audio feedback coming from different directions, test participants are able to correctly determine whether the two audio tracks are different or the same, with a success rate of 0.5 or less.

\paragraph{H\textsubscript{a} (Alternative hypothesis):} When comparing audio feedback coming from different directions, test participants are able to correctly determine whether the two audio tracks are different or the same, with a success rate of more than 0.5.

\subsection{Experiment Procedure}
In order to test how distinguishable the eight different kinds of audio feedback are, an experiment with the focus on ascertaining whether participants can discriminate between them is conducted. In this experiment, test participants are asked to compare two audio files and state whether they are identical or the same.

The experiment is an \textit{alternative forced choice (2-AFC)} test. In this type of experiment, participants are presented with two stimuli, and must choose between two responses: either they can discriminate between the two stimuli, or they cannot. The experiment has 24 participants, each of whom listen to eight pairs of audio clips. After listening to each pair, the participants are asked to state whether the two clips were identical or different. The first audio feedback in each pair is the same for one participant, acting as a \textit{reference stimulus}. There are eight possible stimuli; one for each sensor angle on the device. This means that the front sound is simulated to be coming from five different angles, and the back sound is simulated to be coming from three different angles, making eight sound files in total. The front and back sound are described in chapter \ref{ch:implementation}. The reference stimulus varies from participant to participant. The other clip in each of the eight pairs is one of the eight kinds of audio feedback. These act as the \textit{alternative stimuli}, and each of these are compared to the reference stimulus. One of the alternative stimuli is the same as the reference stimulus, and each alternative stimulus is presented to each test participant only once.

\begin{figure}[H]
  \centering
  \includegraphics[width = 0.9 \textwidth]{figures/kejserTest.jpg}
  \caption{Test setup for the experiment for hypothesis 3.}
  \label{fig:kejsertest}
\end{figure}

In order to keep the experiment balanced, the experiment requires each reference stimulus to be tested an equal amount of times. In the case of this experiment, each reference stimulus is tested on three participants, and in total 24 people participated in the experiment, resulting in 192 responses in total. The combination of reference stimulus and test participant are randomized, in order to increase the reliability of the experiment.

\begin{figure}[!h]
  \centering
  \includegraphics[scale=0.4]{figures/3rdExperimentExample.png}
  \caption{Answers from a participant,. Note the reference stimuli on the left (in blue), the alternative stimuli in the middle (in yellow), and the responses on the right. Green responses are correct answers, while red responses are incorrect answers.}
  \label{fig:participantSix}
\end{figure}

The setup for the experiment is one test participant at a time sitting in front of a test facilitator. The role of the facilitator is to introduce the participant to the test, present to them the audio clips they have to compare, and note down the responses. This setup can be seen in figure \ref{fig:kejsertest}. The response for each pair comparison is either \textbf{same} or \textbf{different}, representing the two options the test participants are forced to choose between after having listened to the two audio clips. The results from the experiment are written in a spreadsheet, and it is noted whether the response is correct or incorrect, as can be seen in figure \ref{fig:participantSix}.

\subsection{Analysis of Results}
The experiment resulted in 192 responses from 24 test participants. Out of these 192 responses, 161 are correct, and 31 are incorrect. The distribution of these results can be seen in figure \ref{fig:thirdtestfirstdiagram}.

\begin{figure}[H]
  \centering
  \includegraphics[scale = 0.4]{figures/3testDia1.png}
  \caption{A comparison between the amount of correct responses (green pillar) and incorrect responses (red pillar).}
  \label{fig:thirdtestfirstdiagram}
\end{figure}

Out of the 161 correct responses, 21 pairs of sound clips are correctly deemed identical, and 140 are correctly deemed different. Out of the 31 incorrect responses, 28 pairs are incorrectly deemed identical, and 3 pairs are incorrectly deemed different. The distribution of these results can be seen in figure \ref{fig:thirdtestseconddiagram}.

\begin{figure}[H]
  \centering
  \includegraphics[scale = 0.4]{figures/3testDia2.png}
  \caption{A comparison between the two types of of correct responses (green pillars) and the two types of incorrect responses (red pillars).}
  \label{fig:thirdtestseconddiagram}
\end{figure}

Since the experiment is binomial, and the alternative hypothesis claims that the true probability of a correct response is greater than 0.5, the \texttt{binom.test} function in R can be used to compute the results. The command for doing this is \texttt{binom.test(161,192,0.5,alternative = "greater")}. Computing the results this way shows that the true probability of a correct answer is 0.84, which is greater than 0.5. It also shows that the p-value is less than 2.2e-16, which means that the probability of getting the same results as the experiment yielded (or more extreme results), given that the null hypothesis is true, is less than 2.2e-16. Since this value is very low, the null hypothesis can be rejected, and the alternative hypothesis can be accepted.

\subsection{Conclusion and Discussion of the Results}
From the analysis, it can be concluded that the alternative hypothesis can be accepted. To elaborate, the experiment shows that the audio feedback provided for objects detected in different directions relative to the device is clearly distinguishable from one another. However, there are some things which need to be considered.

All test participants are students at Aalborg University, and are thus relatively young. There is therefore the possibility that their hearing is better than a potential older user of the device. This is just one possible consequence of the fact that the sample is not very diverse. The lack of diversity may damage the validity of the experiment, as it may cause unknown sources of error.

Something which may have an effect on the results is the test environment. The experiment took place in an large open space, where seclusion could only happen by having the experiment take place in corners or behind large objects. While the experiment was conducted in an area with relatively few people, some environmental noise may have interfered with the test, which may have worsened the audio clip discrimination. Having a more secluded and less noisy area might cause less interference and increase the experiment’s reliability.

Some methods are used to help ensure testing validity, namely the high amount of test participants, and the fact that the reference stimuli are balanced. If one were to have a more effective methodology, further options are available:

Different samples of the population could be tested using blocking --- for example, participants could be categorized by gender or age, opening up to the possibility of discovering that certain groups can more easily discriminate between different sounds than others. Different kinds of audio feedback could be tested by the participants, such as audio at different simulated distances. Different kinds of environmental settings could be used for the testing, because while environmental noise might cause interference, it might also represent a more realistic use of the feedback, than in a noiseless secluded area. Expanding the test to include sound coming from several directions at once would result in a more thorough test of the different stimuli. 

For some of the sound comparisons, test participants stated that they can hear no difference, even though two different audio files are presented to them. The most frequent scenario in which this happened is when a test participant is presented with audio feedback for two angles that are 45\textdegree \ apart. This was the case in 19 of the 28 cases where two sound clips are incorrectly deemed identical. This indicates that the case where it is hardest for users to discern between different audio angles is when the angle between them is 45\textdegree.