\chapter{Design of the Bat Prism and its\\Audio Feedback}\label{ch:design}
This chapter goes through the design process needed for the implementation and evaluation of the hypotheses and their attached criteria. The concept for the prototype, as well as the method for reaching it, is described here. The prototype will be referred to as the 'Bat Prism', as its body is the shape of an eight-sided prism, and it utilizes echolocation, similarly to some bats.

\section{Design Methods}
In order to develop a prototype design, the \textit{10 plus 10} design method, as described by Saul Greenberg et al.\nocite{Greenberg2012}, is utilized. When using this method, at least 10 design concepts are created to address the problem formulation. The concepts are sketched and annotated, based on a list of requirements made from the problem formulation, in order to have the idead retain the same focus. This process is comparable to brainstorming, in that the goal is to generate a large quantity of concepts fast, and to not settle on a specific concept immediately.

Once this is done, the concepts are reviewed, their merits and disadvantages are discussed, and the most promising concept is picked. In this case, the picked concept is a physical object surrounded by ultrasonic sensors, using echolocation to determine the distance to an object. These sensors are controlled using an arduino, a piece of open-source hardware, which will be explained further in chapter \ref{ch:implementation}. A crude sketch of this basic concept can be seen in figure \ref{fig:sketch_0}.

\begin{figure}[!h]
   \centering
   \includegraphics[scale=0.1]{figures/sketch_0.jpg}
   \caption{Crude sketch of the basic concept.}
   \label{fig:sketch_0}
\end{figure}

Using this as a starting point, variations of the concept are discussed. In this phase, details are fleshed out. The amount of sensors needed is considered, as well as the shape of the physical object, and what kind of audio feedback should be used. After discussing the variations on the basic concept, a final design is chosen, which can be seen in section \ref{sec:designconcept}.

Throughout this design phase, ideas and variations are sketched, in order to illustrate the concepts. Storyboarding is also utilized to illustrate the user-device interaction step by step, and the context in which it is used. A storyboard illustrating the context of use is seen in figure \ref{fig:storyboard}.

\begin{figure}[!h]
   \centering
   \includegraphics[width = \textwidth]{figures/storyboard.png}
   \caption{Storyboard illustrating the prototype in context of use.}
   \label{fig:storyboard}
\end{figure}

\section{Design Concept}\label{sec:designconcept}
After applying the \textit{10 plus 10} method, a design concept is chosen; a device which can be used to test the creation of cognitive maps in a small-scale testing environment, as it uses echolocation to determine the location of objects relative to the device, and provides audio feedback based on this, in order to to support one’s use of cognitive maps. The idea is that the concept could be expanded to apply to everyday use, by implementing the techniques used in this project into a wearable object, but this type of implementation will not be assessed in this particular project. 

Before the final prototype is conceptualized, a simpler device is created, in order to have a less costly version of the device. This way, the concept can be tested before reiteration. The ‘body’ of this early version is a cuboidal block of wood. This version has only four sensors and can detect obstacles in four directions; front, back, left, and right. The components required for the device to function (such as the Arduino), are nailed directly onto the wooden block, as it is not hollow. This version is shown in figure \ref{fig:prot_wood}.

\begin{figure}[!h]
   \centering
   \includegraphics[scale=1]{figures/prot_wood.png}
   \caption{The wooden prototype with all the components nailed on it.}
   \label{fig:prot_wood}
\end{figure}

\subsection{Audio Feedback}
In order to provide users  with an understanding of their surroundings, non-visual feedback in the form of audio is used, which means that when the device detects an obstacle within its range, a sound will be played. The device is meant to give detailed feedback of the position of the obstacle detected. As mentioned in the background research, non-sighted people use non-visual cues, such as sounds from rustling leaves in a tree. As is seen in section \ref{sec:existing_solutions}, some of the existing solutions also use audio feedback for their device, like the Navbelt and the Miniguide.

It is important to note that the audio file used for the feedback is not important, but rather the way it is manipulated by things such as amplitude. The device is designed with eight directions of detection, allowing for a more detailed feedback of the surroundings, in comparison to using just one sensor. The Bat Prism should be able to detect in eight possible direction at all times, but only give audio feedback when an obstacle is within 15 cm. The audio feedback is expected to vary based on the distance between the device and the obstacle. It should also give audio feedback based on the angle of the obstacle relative to the device. To get this kind of audio feedback, stereo panning is needed --- that is, simulating hearing audio in two dimensions, allowing the user  to pinpoint the location from which the audio is coming. When the device is turned on but no object is detected, a subtle sound will be played to tell the user that the device is now active. This sound should continue to play until another state of the device becomes active (i.e. the device is turned off or detects an object).

In order to create audio feedback in a 360\textdegree \ horizontal area, there needs to be some change in the audio to indicate if the audio is coming from the back or front of the device. The human mind can not tell the difference between audio coming from the front and the back, when given no context (Blauert, 2016\nocite{spatialHearing}). In order to avoid this confusion, the audio feedback for detected obstacles around the device is split into two. The front area of the device consists of 270\textdegree \ out of the total 360\textdegree \ available. In figure \ref{fig:audioSpectrum}, the circle represents the device's range of detection, and the red and green pillars each correspond to a sensor. The green pillars represent the sensors which will result in audio feedback indicating detection in the front area, and the red pillars represent the sensors which will result in audio indicating detection from behind. This covers the audio feedback enabling the user to pinpoint the location of the audio. In order to tell the difference between an obstacle being close or far away, the audio needs to adjust accordingly. This is done using low amplitude audio for far away obstacles and high amplitude audio for close obstacles, as well as varying degrees of reverb and a lowpass filter.

\begin{figure}[!h]
   \centering
   \includegraphics[width = 0.7 \textwidth]{figures/audioSpectrum.png}
   \caption{The Bat Prism's detection range, seen from above.}
   \label{fig:audioSpectrum}
\end{figure}

\subsection{Audio Map}
The detailed audio feedback gives the user information of obstacles within the device’s range, in order to help enhance the creation and use of cognitive maps. In order to prepare the user for their journey ahead, the concept of audio maps will be implemented. The idea of an audio map is to record all the audio feedback the device will give throughout a journey following a specific route. This recording is done using an external computer, so that the audio map can be stored and replayed for the user. The user would listen to this audio map of the route before travelling. The audio map will be affected by the audio feedback, since the recording is based on it.

\subsection{The Model}
As stated before, the prototype is meant to be a platform for testing audio maps. A sketch of the final design is shown in figure \ref{fig:prism_sketch}.

\newpage
\begin{figure}[!h]
   \centering
   \includegraphics[scale=0.4]{figures/prism_sketch.png}
   \caption{Sketch of the design concept.}
   \label{fig:prism_sketch}
\end{figure}

A hollow prism-shaped 3D-printed object with octagonal bases and a closed bottom serves as the \textit{body} of the device, keeping all of the device’s components in place. This shape was chosen to make eight surfaces, each having enough room for one sensor, so that the sensors would equally cover the prism’s sides. Each surface has two holes, in order to make room for both the transmitter and the receiver of the sensors.
The eight ultrasonic sensors use echolocation to determine the distance to an object. These sensors are 45\textdegree \ apart, providing eight distinct angles in which objects can be detected. 
The following components are also in the device: an Arduino Mega; a 3.3 V battery, along with a power booster to boost it to 5 V; and a bluetooth modem. A computer can then connect to the device's bluetooth modem using its own bluetooth modem, and receive the data, eliminating the need for wires connecting the device to a computer. A headset is connected to the computer, in order for the user to receive audio feedback. 

\section{Product Requirement}
The focus for this product is the creation of audio maps to help non-sighted people in planning routes and increase their spatial understanding via cognitive mapping. The prototype should be built to be operated by hand. To ensure that the study curriculum and the final problem formulation are being fulfilled, a more in-depth product requirement list is created.   

\subsection{Software}
In order to program a connection from each of the components to the device, a software element has to be added to the Arduino which acts as the brain for the whole construction. The requirements for the software to work properly are:
\begin{enumerate}
\item The program has to be able to process the input from the ultrasonic sound signals and accumulate distance and angle with an acceptable failure rate of 95\%.
\item The software needs to extract data from the ultrasonic sensors.
\item The software needs to be able to detect obstacles in a range of 15 cm.
\item The software needs to be able to change the audio feedback by applying audio processing techniques, such as stereo-panning and pitch change.
\item The software needs to give audio feedback to tell which state it is currently in.
\begin{enumerate}
\item Off state, when the device is turned off (no audio).
\item On state, when the device is turned on (subtle sound).
\item On/detecting state, when the device is turned on and detecting an obstacle (audio feedback using panning and distance simulation).
\end{enumerate}

\subsection{Audio Feedback}
In order to fulfill the audio processing requirement for this semester project, the implementation of audio filters needs to be applied. A clear definition of what sound is going to be used needs to be declared, and what filters will be applied to the audio feedback.
\item The audio feedback needs to be distinguishable by the user depending on the detection of obstacles in a given environment.
\item The audio feedback needs to change depending on the distance between the Bat Prism and a detected object. The further away the object is, the lower the amplitude of the feedback should be. A reverb and a lowpass filter should also be utilized to simulate distance.
\item The audio feedback for communicating the angle to an obstacle needs to be based on stereo panning.
\item The device has to give audio based feedback when the device is in a state of not detecting any obstacles. This audio feedback should be a low-amplitude, subtle sound.
\end{enumerate}

Once a design concept is in place and the product requirements are set, the next section will explain how the concepts will be implemented in conjunction with the electronic components, the Arduino and the Pure Data code.