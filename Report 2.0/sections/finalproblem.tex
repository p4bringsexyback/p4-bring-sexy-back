\chapter{Final Problem}\label{ch:finalproblem}

\section{Finding the Problem}
In addressing the initial problem statement, the research questions are answered as follows: \\
\\\textit{What methods do non-sighted people use to determine their surroundings and navigate in their daily lives?}\\
As Passini et al.’s experiment and Quiñones et al.'s study shows, non-sighted people make use of cognitive mapping in order to obtain a spatial understanding of their surroundings. The cognitive map is created by identifying non-visual cues in an environment, namely audio and tactile cues. The cognitive map is supported by the planning of a route, since it helps non-sighted people memorize the cues on a journey. By utilizing these cognitive maps, a non-sighted person is able to achieve a spatial understanding of an environment, despite not having visual input.\\
\\\textit{What solutions are currently available to non-sighted people which assist them in determining their surroundings?}\\
Besides the white cane and the guide dog, several other solutions have been developed: the Navbelt, the GuideCane, the UltraCane, and the Miniguide. So far, none of these solutions have replaced the cane or the guide dog entirely. The similarity between these solutions is that they assist non-sighted individuals in-route. However, they do not assist non-sighted people in the planning stage, as there is no way to obtain non-visual cues using the current solutions except for in-route.\\
\\\textit{How can audio processing be utilised in such a way that it can help a non-sighted person to determine their surroundings?} \\
As shown in the solutions mentioned in chapter \ref{ch:background_research}, in some cases, non-visual feedback is utilized in the form of audio. Examples of audio feedback are a cycle-based beeping, a constant but pitch-changing tone, or a radar-inspired sweep. Based on this, it is reasonable to assume that audio processing can be used to help non-sighted individuals determine their surroundings. As has been addressed in chapter \ref{ch:background_research}, non-sighted people in unfamiliar environments prefer to be relayed a lot of information about their surroundings, so covering a wide angular area may be ideal. Furthermore, stereophonic audio panning as well as a varying pitch can be used to communicate the location of an object.\\
\\
While many of the solutions discussed in chapter \ref{ch:background_research} attempt to improve obstacle avoidance for non-sighted people, there is a lack of solutions which focus on the need for non-sighted people to prepare their journey and create cognitive maps. For this reason, it may prove beneficial to shift focus from merely obstacle avoidance, to assisting non-sighted people in travel preparation and the creation of cognitive maps. The aim to use audio processing to achieve this goal remains. A final problem formulation is based on this.

\section{Final Problem Formulation}
In Passini et al.’s experiment, they observed that non-sighted people plan their travels in greater detail than sighted people do (Passini et al., 1988, pp. 236-237\nocite{Passini1988}), and while they make more errors in following a route than sighted people, they are able to have a spatial understanding of a route as shown in their re-creation of it, despite their lack of visual cues of the environments (Passini et al., 1988, p. 247\nocite{Passini1988}). \textit{Can a non-sighted person improve their spatial understanding and ability to follow a route if they make use of a sensory artifact implementing audio feedback, after having listened to an audio map of the environment?} \\
\\
An audio map is a pre-recorded audio file of the route, performed with no sudden encounters with obstacles, that the user of the device can listen to before they go through the designated environment. The audio map is an aid for the user that helps them remember the ‘sound’ of the route they are going to go through before they try to traverse it. The audio map helps to create a cognitive map in the minds of users while they listen to it. With the help of the device, they should be able to find the right path through the environment.\\
\\
This problem requires three hypotheses, each with their own success criteria, since both the device's technical performance and the map's influence will be tested. The three hypotheses are as follows:

\paragraph{Hypothesis 1 (testing the device):} The device is able to determine the location of an object relative to itself, both by angle and distance.

\paragraph{Hypothesis 2 (testing the audio map):} Non-sighted individuals who listened to the audio map in advance are better at following a route while using the device, than those who did not listen to the audio map.

\paragraph{Hypothesis 3 (testing the audio feedback):} The audio feedback provided for different directions of obstacles relative to the device is clearly distinguishable from one another.

\subsection{Success Criteria for the Device}
In order to accept hypothesis 1, the following success criteria need to be met:
\begin{enumerate}
\item The device must be equipped with electronic components to calculate the distance to objects surrounding it.
\item The electronic components must be arranged in such a way that objects can be detected in an area of 360 horizontal degrees.
\item The device must be able to broadcast the distance and angle for objects detected.
\end{enumerate}

\subsection{Success Criteria for the Audio Map}
In order to accept hypothesis 2, the following success criteria need to be met:
\begin{enumerate}
\item The audio map needs to be able to reasonably communicate environmental information about a route to the user in the form of sound. The sound in the audio map should be recognized to be the same as in the real-time feedback.
\item After having listened to the map, the user should be able to follow an intended route better than those who did not listen to the map.
\item The prototype must have real-time audio feedback, which must represent the non-visual cues needed by non-sighted people.
\item The real-time feedback must come from detection and computing of the device's surroundings in a spectrum of 360 horizontal degrees, so the user of the device is able to tell where objects in the environment are, and how close they are.
\item The prototype should be able to record a track from its real-time feedback, in order for it to be listened to as an audio map.
\end{enumerate}

\subsection{Success Criteria for the Audio Feedback}
In order to accept hypothesis 3, the following success criteria need to be met:
\begin{enumerate}
\item Users should be able to tell if they are listening to the same or different kinds of audio feedback, when comparing them to each other.
\item Users should be able to tell what direction the audio feedback represents by pinpointing the direction of the virtual sound source.
\end{enumerate}
\bigskip In this project, a device is created which can assist non-sighted people in their cognitive mapping, both in-route and in the planning stage before traversing a route. This means that the device's function is twofold: firstly, it should communicate information about the environment of a route to the user before the journey is started, so that the user can build a cognitive map of the environment; secondly, it should provide real-time cues to the user as they are on the route. These cues will be audio-only, and are based on information gathered by ultrasonic sensors. The device created will not be designed for real-life use, but will rather be a small-scale prototype to use for testing the principle of audio maps. In other words, the device is not designed to be a user-friendly product, but rather a platform for developing and testing audio maps. Based on this chapter’s content, the next chapter will follow up on designing and implementing a solution for the problem; a sensory device implementing audio feedback and an audio map.